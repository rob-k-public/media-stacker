from troposphere.cloudformation import AWSCustomObject


class ElasticTranscoderPipeline(AWSCustomObject):
    resource_type = "Custom::ElasticTranscoderPipeline"

    props = {
        'ServiceToken': (str, True),
        'Name': (str, True),
        'Role': (str, True),
        'InputBucket': (str, True),
        'OutputBucket': (str, False),
        'AwsKmsKeyArn': (str, False),
        'ContentConfig': (object, False),
        'ThumbnailConfig': (object, False),
        'Notifications': (object, False),
    }

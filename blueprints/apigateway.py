from stacker.blueprints.base import Blueprint
from troposphere import apigateway, Ref, GetAtt, Output, Join, ImportValue
from troposphere.apigateway import Resource, Method, Integration, IntegrationResponse, MethodResponse, Deployment, \
    Stage, ApiKey, StageKey, UsagePlan, QuotaSettings, ThrottleSettings, ApiStage, UsagePlanKey
import json


class RestApi(Blueprint):
    VARIABLES = {
        "LambdaExecutionRole": {
            "type": str,
            "description": "Arn of the role to execute the lambda with."
        },
        "LambdaFunction": {
            "type": str,
            "description": "Arn of the lambda to execute."
        },
        "Description": {
            "type": str,
            "description": "Description of the API Gateway."
        },
        "Path": {
            "type": str,
            "description": "The path part for the API resource."
        },
        "RequestParams": {
            "type": list,
            "description": "List of request parameters to pass to the lambda."
        }
    }

    def create_template(self):
        template = self.template
        variables = self.get_variables()

        template.add_description("API Gateway Endpoint (Created with Stacker)")

        rest_api = template.add_resource(apigateway.RestApi(
            "RestApi",
            Name=self.name,
            Description=variables["Description"],
            Policy={
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": [
                            "execute-api:Invoke"
                        ],
                        "Resource": [
                            "*"
                        ],
                        "Principal": "*"
                    }
                ]
            },
        ))

        resource = template.add_resource(Resource(
            "LambdaResource",
            RestApiId=Ref(rest_api),
            PathPart=variables["Path"],
            ParentId=GetAtt("RestApi", "RootResourceId"),
        ))

        template_mappings = {}
        for param in variables["RequestParams"]:
            template_mappings[param] = "$input.params('{param}')".format(param=param)

        method = template.add_resource(Method(
            "LambdaMethod",
            RestApiId=Ref(rest_api),
            AuthorizationType="NONE",
            ResourceId=Ref(resource),
            HttpMethod="GET",
            Integration=Integration(
                Credentials=ImportValue(variables["LambdaExecutionRole"]),
                Type="AWS",
                IntegrationHttpMethod='POST',
                IntegrationResponses=[
                    IntegrationResponse(
                        StatusCode='200'
                    )
                ],
                Uri=Join("", [
                    "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
                    ImportValue(variables["LambdaFunction"]),
                    "/invocations"
                ]),
                RequestTemplates={
                    "application/json": json.dumps(template_mappings)
                }
            ),
            MethodResponses=[
                MethodResponse(
                    "MethodResponse",
                    StatusCode='200'
                )
            ]
        ))

        stage_name = 'v1'

        deployment = template.add_resource(Deployment(
            "%sDeployment" % stage_name,
            DependsOn="LambdaMethod",
            RestApiId=Ref(rest_api),
        ))

        stage = template.add_resource(Stage(
            '%sStage' % stage_name,
            StageName=stage_name,
            RestApiId=Ref(rest_api),
            DeploymentId=Ref(deployment)
        ))

        key = template.add_resource(ApiKey(
            "ApiKey",
            StageKeys=[StageKey(
                RestApiId=Ref(rest_api),
                StageName=Ref(stage)
            )]
        ))

        usage_plan = template.add_resource(UsagePlan(
            "UsagePlan",
            UsagePlanName="UsagePlan",
            Description="usage plan",
            Quota=QuotaSettings(
                Limit=50000,
                Period="MONTH"
            ),
            Throttle=ThrottleSettings(
                BurstLimit=500,
                RateLimit=5000
            ),
            ApiStages=[
                ApiStage(
                    ApiId=Ref(rest_api),
                    Stage=Ref(stage)
                )]
        ))

        usage_plan_key = template.add_resource(UsagePlanKey(
            "UsagePlanKey",
            KeyId=Ref(key),
            KeyType="API_KEY",
            UsagePlanId=Ref(usage_plan)
        ))

        template.add_output([
            Output(
                "ApiEndpoint",
                Value=Join("", [
                    "https://",
                    Ref(rest_api),
                    ".execute-api.eu-west-1.amazonaws.com/",
                    stage_name
                ]),
                Description="Endpoint for this stage of the api"
            ),
            Output(
                "ApiKey",
                Value=Ref(key),
                Description="API key"
            ),
        ])

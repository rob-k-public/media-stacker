from stacker.blueprints.base import Blueprint
from troposphere import dynamodb
from troposphere.dynamodb import KeySchema, ProvisionedThroughput, AttributeDefinition


class Table(Blueprint):
    VARIABLES = {
        "KeySchema": {
            "type": list,
            "description": "List of attributes that make up the primary key."
        },
        "ReadCapacityUnits": {
            "type": int,
            "description": "Number of consistent reads per second.",
            "default": 5
        },
        "WriteCapacityUnits": {
            "type": int,
            "description": "Number of consistent writes per second.",
            "default": 5
        },
        "AttributeDefinitions": {
            "type": dict,
            "description": "Map of definitions for attributes that make up the key."
                           "Key is the name of the attribute, value is the type (S, N, B)"
                           "S for string, N for numeric, B for binary"
        }
    }

    def create_template(self):
        template = self.template
        variables = self.get_variables()

        template.add_description("DynamoDB Table (Created with stacker)")

        template.add_resource(dynamodb.Table(
            "DynamoDbTable",
            TableName=self.name,
            AttributeDefinitions=list(map(lambda x: AttributeDefinition(
                AttributeName=x[0],
                AttributeType=x[1],
            ), variables["AttributeDefinitions"].items())),
            KeySchema=list(map(lambda x: KeySchema(AttributeName=x, KeyType="HASH"), variables["KeySchema"])),
            ProvisionedThroughput=ProvisionedThroughput(
                ReadCapacityUnits=variables["ReadCapacityUnits"],
                WriteCapacityUnits=variables["WriteCapacityUnits"],
            ),
        ))

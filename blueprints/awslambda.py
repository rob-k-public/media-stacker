from stacker.blueprints.base import Blueprint
from troposphere import awslambda, Join, GetAtt, Output, Export, Sub
from troposphere.awslambda import Code
from troposphere.iam import Role, Policy


class Function(Blueprint):
    VARIABLES = {
        "Runtime": {
            "type": str,
            "description": "Runtime for the lambda function."
        },
        "CodeS3Bucket": {
            "type": str,
            "description": "Bucket that contains source code for the lambda."
        },
        "CodeS3Key": {
            "type": str,
            "description": "Key of the source code file in S3."
        },
        "Handler": {
            "type": str,
            "description": "The handler method of the lambda function.",
            "default": "index.handler"
        },
        "Policies": {
            "type": dict,
            "description": "Dict for <Action, Resource> pairs for extra policies on lambda.",
            "default": dict()
        }
    }

    def create_template(self):
        template = self.template
        variables = self.get_variables()

        template.add_description("AWS Lambda Function (Created with Stacker)")

        statements = [{
            "Action": ["logs:*"],
            "Resource": "arn:aws:logs:*:*:*",
            "Effect": "Allow"
        }, {
            "Action": ["lambda:*"],
            "Resource": "*",
            "Effect": "Allow"
        }]

        if [variables["Policies"]]:
            for action, resource in variables["Policies"].items():
                statements.append({
                    "Action": [action],
                    "Resource": resource,
                    "Effect": "Allow"
                })

        template.add_resource(Role(
            "LambdaExecutionRole",
            Path="/",
            Policies=[Policy(
                PolicyName="root",
                PolicyDocument={
                    "Version": "2012-10-17",
                    "Statement": statements
                })],
            AssumeRolePolicyDocument={"Version": "2012-10-17", "Statement": [
                {
                    "Action": ["sts:AssumeRole"],
                    "Effect": "Allow",
                    "Principal": {
                        "Service": [
                            "lambda.amazonaws.com",
                            "apigateway.amazonaws.com"
                        ]
                    }
                }
            ]},
        ))

        lambda_function = template.add_resource(awslambda.Function(
            "LambdaFunction",
            Code=Code(
                S3Bucket=variables["CodeS3Bucket"],
                S3Key=variables["CodeS3Key"],
            ),
            Handler=variables["Handler"],
            Role=GetAtt("LambdaExecutionRole", "Arn"),
            Runtime=variables["Runtime"],
        ))

        template.add_output([
            Output(
                "LambdaExecutionRoleArn",
                Value=GetAtt("LambdaExecutionRole", "Arn"),
                Description="ARN of the lambda execution role.",
                Export=Export(Sub(self.name + "-" + "LambdaExecutionRoleArn"))
            ),
            Output(
                "LambdaFunctionArn",
                Value=GetAtt("LambdaFunction", "Arn"),
                Description="ARN of the lambda function.",
                Export=Export(Sub(self.name + "-" + "LambdaFunctionArn"))
            ),
        ])

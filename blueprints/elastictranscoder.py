from stacker.blueprints.base import Blueprint
from troposphere import GetAtt
from troposphere.iam import Role, Policy

from custom_resources import elastictranscoder


class ElasticTranscoderPipeline(Blueprint):
    VARIABLES = {
        "ServiceToken": {
            "type": str,
            "description": "ARN of the lambda handling the ElasticTranscoder cfn updates."
        },
        "InputBucket": {
            "type": str,
            "decription": "The bucket that contains input content for the pipeline."
        },
        "OutputBucket": {
            "type": str,
            "decription": "The bucket that contains input content for the pipeline."
        },
        "PipelineName": {
            "type": str,
            "description": "The name of the ElasticTranscoder Pipeline."
        }
    }

    def create_template(self):
        template = self.template
        variables = self.get_variables()

        template.add_description("Elastic Transcoder Pipeline (Created with Stacker)")

        transcoder_role = template.add_resource(Role(
            "ElasticTranscoderPipelineRole",
            Policies=[Policy(
                PolicyName="ElasticTranscoderExecutionPolicy",
                PolicyDocument={
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Action": [
                                "logs:CreateLogGroup",
                                "logs:CreateLogStream",
                                "logs:PutLogEvents"
                            ],
                            "Resource": "arn:aws:logs:*:*:*"
                        },
                        {
                            "Effect": "Allow",
                            "Action": [
                                "elastictranscoder:*",
                                "iam:PassRole"
                            ],
                            "Resource": [
                                "*"
                            ]
                        },
                        {
                            "Effect": "Allow",
                            "Action": [
                                "s3:*"
                            ],
                            "Resource": [
                                "arn:aws:s3:::" + variables["InputBucket"],
                                "arn:aws:s3:::" + variables["InputBucket"] + "/*"
                            ]
                        }
                    ]
                },
            )],
            AssumeRolePolicyDocument={"Version": "2012-10-17", "Statement": [
                {
                    "Action": ["sts:AssumeRole"],
                    "Effect": "Allow",
                    "Principal": {
                        "Service": [
                            "lambda.amazonaws.com",
                            "elastictranscoder.amazonaws.com"
                        ]
                    }
                }
            ]},
        ))

        template.add_resource(elastictranscoder.ElasticTranscoderPipeline(
            "ElasticTranscoderPipeline",
            ServiceToken=variables["ServiceToken"],
            Name=variables["PipelineName"],
            Role=GetAtt(transcoder_role, "Arn"),
            InputBucket=variables["InputBucket"],
            OutputBucket=variables["OutputBucket"],
        ))

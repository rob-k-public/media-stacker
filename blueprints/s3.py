from stacker.blueprints.base import Blueprint
from troposphere import s3
from troposphere.s3 import CorsConfiguration, CorsRules


class Bucket(Blueprint):
    VARIABLES = {
        "OpenCorsConfiguration": {
            "type": bool,
            "description": "Add default CORS configuration with GET for * ?",
            "default": False
        }
    }

    def create_template(self):
        template = self.template
        variables = self.get_variables()

        template.add_description("S3 Bucket (Created with Stacker)")

        bucket = s3.Bucket("S3Bucket", BucketName=self.name)

        if variables["OpenCorsConfiguration"]:
            bucket.CorsConfiguration = CorsConfiguration(
                CorsRules=[
                    CorsRules(
                        AllowedHeaders=["*"],
                        AllowedMethods=["GET"],
                        AllowedOrigins=["*"],
                    )
                ]
            )

        template.add_resource(bucket)
